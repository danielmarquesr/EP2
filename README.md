# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

Tutorial de Uso:
1- Tenha na sua máquina o Java 1.8 instalado.

2- Tenha uma IDE Java instalado em sua máquina (Netbeans ou Eclipse).

3- Na IDE selecione o projeto para abrir.

4- Clique no botão para executar o projeto, caso peça a main, ela está localizada na classe TelaLogin.java na package view.

5- Com o programa em execução entre como empregado:
	Usuário: usuario
	Senha: 1234

6- Abrirá a tela de Pedidos, mas antes deve ser colocado algo no estoque para a sua utilização. No canto esquerdo superior há uma opção para abrir a tela do estoque, clique nela.

7- Com os três campos preenchidos (nome, preço e quantidade) clique em um dos três botões do lado direito para adicionar o que deseja, a tabela abaixo será atualizada com o produto adicionado. Terminado o processo de adicionamento de produtos no estoque pode fechar a janela.

8- De volta a tela de Pedidos, preencha os campos com os detalhes do pedido do cliente e adicione o pedido clicando no botão "Adicionar Pedido".

9- Irá aparecer o nome do cliente na lista abaixo, dê dois cliques no nome da lista para mais informações sobre o pedido.

OBS: caso não tenha a quantidade desejada no estoque irá aparecer um alerta de estoque baixo.
